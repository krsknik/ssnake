// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SsnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SSNAKE_API ASsnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
