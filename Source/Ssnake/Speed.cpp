// Fill out your copyright notice in the Description page of Project Settings.


#include "Speed.h"
#include "SnakeBase.h"

// Sets default values
ASpeed::ASpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetActorTickInterval(0.2);
			Destroy();
		}
	}
}

