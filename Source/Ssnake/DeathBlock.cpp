// Fill out your copyright notice in the Description page of Project Settings.


#include "DeathBlock.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ADeathBlock::ADeathBlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADeathBlock::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADeathBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADeathBlock::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			for (int i = 0; i < Snake->SnakeElements.Num(); ++i)
			{
				Snake->SnakeElements[i]->Destroy();
			}
			Snake->Destroy();
		}
	}
}

