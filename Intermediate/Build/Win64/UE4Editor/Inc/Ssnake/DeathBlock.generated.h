// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SSNAKE_DeathBlock_generated_h
#error "DeathBlock.generated.h already included, missing '#pragma once' in DeathBlock.h"
#endif
#define SSNAKE_DeathBlock_generated_h

#define Ssnake_Source_Ssnake_DeathBlock_h_13_SPARSE_DATA
#define Ssnake_Source_Ssnake_DeathBlock_h_13_RPC_WRAPPERS
#define Ssnake_Source_Ssnake_DeathBlock_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Ssnake_Source_Ssnake_DeathBlock_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADeathBlock(); \
	friend struct Z_Construct_UClass_ADeathBlock_Statics; \
public: \
	DECLARE_CLASS(ADeathBlock, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Ssnake"), NO_API) \
	DECLARE_SERIALIZER(ADeathBlock) \
	virtual UObject* _getUObject() const override { return const_cast<ADeathBlock*>(this); }


#define Ssnake_Source_Ssnake_DeathBlock_h_13_INCLASS \
private: \
	static void StaticRegisterNativesADeathBlock(); \
	friend struct Z_Construct_UClass_ADeathBlock_Statics; \
public: \
	DECLARE_CLASS(ADeathBlock, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Ssnake"), NO_API) \
	DECLARE_SERIALIZER(ADeathBlock) \
	virtual UObject* _getUObject() const override { return const_cast<ADeathBlock*>(this); }


#define Ssnake_Source_Ssnake_DeathBlock_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADeathBlock(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADeathBlock) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADeathBlock); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeathBlock); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADeathBlock(ADeathBlock&&); \
	NO_API ADeathBlock(const ADeathBlock&); \
public:


#define Ssnake_Source_Ssnake_DeathBlock_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADeathBlock(ADeathBlock&&); \
	NO_API ADeathBlock(const ADeathBlock&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADeathBlock); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeathBlock); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADeathBlock)


#define Ssnake_Source_Ssnake_DeathBlock_h_13_PRIVATE_PROPERTY_OFFSET
#define Ssnake_Source_Ssnake_DeathBlock_h_10_PROLOG
#define Ssnake_Source_Ssnake_DeathBlock_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ssnake_Source_Ssnake_DeathBlock_h_13_PRIVATE_PROPERTY_OFFSET \
	Ssnake_Source_Ssnake_DeathBlock_h_13_SPARSE_DATA \
	Ssnake_Source_Ssnake_DeathBlock_h_13_RPC_WRAPPERS \
	Ssnake_Source_Ssnake_DeathBlock_h_13_INCLASS \
	Ssnake_Source_Ssnake_DeathBlock_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Ssnake_Source_Ssnake_DeathBlock_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ssnake_Source_Ssnake_DeathBlock_h_13_PRIVATE_PROPERTY_OFFSET \
	Ssnake_Source_Ssnake_DeathBlock_h_13_SPARSE_DATA \
	Ssnake_Source_Ssnake_DeathBlock_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Ssnake_Source_Ssnake_DeathBlock_h_13_INCLASS_NO_PURE_DECLS \
	Ssnake_Source_Ssnake_DeathBlock_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SSNAKE_API UClass* StaticClass<class ADeathBlock>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Ssnake_Source_Ssnake_DeathBlock_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
