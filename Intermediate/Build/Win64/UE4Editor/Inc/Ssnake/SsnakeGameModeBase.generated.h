// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SSNAKE_SsnakeGameModeBase_generated_h
#error "SsnakeGameModeBase.generated.h already included, missing '#pragma once' in SsnakeGameModeBase.h"
#endif
#define SSNAKE_SsnakeGameModeBase_generated_h

#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_SPARSE_DATA
#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_RPC_WRAPPERS
#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASsnakeGameModeBase(); \
	friend struct Z_Construct_UClass_ASsnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASsnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Ssnake"), NO_API) \
	DECLARE_SERIALIZER(ASsnakeGameModeBase)


#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASsnakeGameModeBase(); \
	friend struct Z_Construct_UClass_ASsnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASsnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Ssnake"), NO_API) \
	DECLARE_SERIALIZER(ASsnakeGameModeBase)


#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASsnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASsnakeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASsnakeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASsnakeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASsnakeGameModeBase(ASsnakeGameModeBase&&); \
	NO_API ASsnakeGameModeBase(const ASsnakeGameModeBase&); \
public:


#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASsnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASsnakeGameModeBase(ASsnakeGameModeBase&&); \
	NO_API ASsnakeGameModeBase(const ASsnakeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASsnakeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASsnakeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASsnakeGameModeBase)


#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_12_PROLOG
#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_SPARSE_DATA \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_RPC_WRAPPERS \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_INCLASS \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_SPARSE_DATA \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Ssnake_Source_Ssnake_SsnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SSNAKE_API UClass* StaticClass<class ASsnakeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Ssnake_Source_Ssnake_SsnakeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
