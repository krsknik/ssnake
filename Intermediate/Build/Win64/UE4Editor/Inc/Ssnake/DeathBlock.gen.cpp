// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Ssnake/DeathBlock.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDeathBlock() {}
// Cross Module References
	SSNAKE_API UClass* Z_Construct_UClass_ADeathBlock_NoRegister();
	SSNAKE_API UClass* Z_Construct_UClass_ADeathBlock();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Ssnake();
	SSNAKE_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ADeathBlock::StaticRegisterNativesADeathBlock()
	{
	}
	UClass* Z_Construct_UClass_ADeathBlock_NoRegister()
	{
		return ADeathBlock::StaticClass();
	}
	struct Z_Construct_UClass_ADeathBlock_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADeathBlock_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Ssnake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeathBlock_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DeathBlock.h" },
		{ "ModuleRelativePath", "DeathBlock.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ADeathBlock_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ADeathBlock, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADeathBlock_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADeathBlock>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADeathBlock_Statics::ClassParams = {
		&ADeathBlock::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADeathBlock_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADeathBlock_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADeathBlock()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADeathBlock_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADeathBlock, 1634292890);
	template<> SSNAKE_API UClass* StaticClass<ADeathBlock>()
	{
		return ADeathBlock::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADeathBlock(Z_Construct_UClass_ADeathBlock, &ADeathBlock::StaticClass, TEXT("/Script/Ssnake"), TEXT("ADeathBlock"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADeathBlock);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
